<?php

class Mobil
{
  protected $bahan_bakar = 0;
  protected $odometer = 0;

  public function lihatBahanBakar()
  {
    return $this->bahan_bakar;
  }

  public function isiBahanBakar($bahan_bakar)
  {
    $this->bahan_bakar = $bahan_bakar;
  }

  protected function mesinBekerja()
  {
    $this->bahan_bakar--;
    $this->odometer++;

    echo "mobil berjalan, 
    kondisi bahan bakar : <b> $this->bahan_bakar </b>, 
    odometer saat ini : <b> $this->odometer </b> <br>";
  }

  public function berhenti()
  {
    echo "mobil berhenti cit cit cit";
  }

  public function jalan()
  {
    if($this->bahan_bakar > 0){
      $this->mesinBekerja();
    } else {
      echo "mobil kehabisan bahan bakar <br>";
    }
    
  }

}
